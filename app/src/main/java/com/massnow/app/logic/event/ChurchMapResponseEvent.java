package com.massnow.app.logic.event;

import com.massnow.app.data.entity.Church;

import java.util.List;
import java.util.Map;

/**
 * ChurchMapResponseEvent
 *
 * @author Jakub Zitny <t102012001@ntut.edu.tw>
 * @since Jun 11 05:54 2014
 */
public class ChurchMapResponseEvent {

    private Map<String, Church> mChurches;

    public static ChurchMapResponseEvent create (Map<String, Church> churches) {
        ChurchMapResponseEvent churchMapResponseEvent = new ChurchMapResponseEvent();
        churchMapResponseEvent.setChurches(churches);
        return churchMapResponseEvent;
    }

    public Map<String, Church> getChurches() {
        return mChurches;
    }

    public void setChurches(Map<String, Church> mChurches) {
        this.mChurches = mChurches;
    }
}
