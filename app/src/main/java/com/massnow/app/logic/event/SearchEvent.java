package com.massnow.app.logic.event;

/**
 * SearchEvent
 *
 * @author Jakub Zitny <t102012001@ntut.edu.tw>
 * @since Jun 07 01:42 2014
 */
public class SearchEvent {

    private String mQuery;

    public static SearchEvent create (String query) {
        return new SearchEvent(query);
    }

    private SearchEvent(String query) {
        mQuery = query;
    }

    public String getQuery() {
        return mQuery;
    }

    public void setQuery(String mQuery) {
        this.mQuery = mQuery;
    }
}
