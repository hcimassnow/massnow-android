package com.massnow.app.logic.di;

import com.massnow.app.view.MainActivity;
import com.massnow.app.MassNowApplication;
import com.massnow.app.view.fragment.ChurchListFragment;
import dagger.Module;
import dagger.ObjectGraph;

/**
 * NOT USED YET!!! IGNORE THIS!
 * dependency injection module for dagger lib
 * provides singleton controller for activity
 * and more?
 * @author Jakub Zitny <t102012001@ntut.edu.tw>
 * @since May 19 5:25 AM 2014
 */
@Module(
    injects = {
        MassNowApplication.class,
        ChurchListFragment.class,
        MainActivity.class
    },
    library = true
)
public class DaggerModule {

    private static ObjectGraph graph;

    /**
     * provide churchcollectioncontrol for churchcollectionfragment
     * @return
     */
//    @Provides
//    @Singleton
//    public ChurchCollectionControl provideChurchCollectionControl() {
//        return new ChurchCollectionControl();
//    }

    /**
     * give access to graph to whole whatever it is
     * having this module injecting something
     * @return
     */
    public static ObjectGraph getObjectGraph() {
        if (graph == null) {
            graph = ObjectGraph.create(new DaggerModule());
        }
        return graph;
    }

}
