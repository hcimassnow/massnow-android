package com.massnow.app.logic.event;

/**
 * ToastEvent
 *
 * @author Jakub Zitny <t102012001@ntut.edu.tw>
 * @since Jun 06 19:23 2014
 */
public class ToastEvent {

    private String mMessage;

    public static ToastEvent create (String message) {
        return new ToastEvent(message);
    }

    private ToastEvent(String message) {
        mMessage = message;
    }

    public String getMessage () {
        return mMessage;
    }

}
