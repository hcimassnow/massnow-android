package com.massnow.app.logic.event;

/**
 * RedrawMenuEvent
 *
 * @author Jakub Zitny <t102012001@ntut.edu.tw>
 * @since Jun 11 15:59 2014
 */
public class RedrawMenuEvent {

    private RedrawMenuEventIcon mIcon;

    public static RedrawMenuEvent create (RedrawMenuEventIcon icon) {
        RedrawMenuEvent e = new RedrawMenuEvent();
        e.setIcon(icon);
        return e;
    }

    public RedrawMenuEventIcon getIcon() {
        return mIcon;
    }

    public void setIcon(RedrawMenuEventIcon mIcon) {
        this.mIcon = mIcon;
    }

    public enum RedrawMenuEventIcon {
        LIST,
        MAP,
        SEARCH
    }

}
