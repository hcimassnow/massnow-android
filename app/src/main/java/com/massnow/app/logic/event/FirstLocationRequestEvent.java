package com.massnow.app.logic.event;

/**
 * FirstLocationRequestEvent
 *
 * @author Jakub Zitny <t102012001@ntut.edu.tw>
 * @since Jun 11 05:49 2014
 */
public class FirstLocationRequestEvent {

    public static FirstLocationRequestEvent create () {
        return new FirstLocationRequestEvent();
    }

}
