package com.massnow.app.logic.event;

import com.massnow.app.data.entity.Church;

import java.util.List;
import java.util.Map;

/**
 * LoadChurchesFinishedEvent
 *
 * @author Jakub Zitny <t102012001@ntut.edu.tw>
 * @since Jun 06 14:22 2014
 */
public class LoadChurchesFinishedEvent {

    private LoadChurchesFinishedMode mMode;
    private Map<String, Church> mGotChurches;

    /**
     * factory method
     * @return event instance
     */
    public static LoadChurchesFinishedEvent create (Map<String, Church> gotChurches,
            LoadChurchesFinishedMode mode) {
        LoadChurchesFinishedEvent event = new LoadChurchesFinishedEvent(gotChurches);
        event.setMode(mode);
        return event;
    }

    private LoadChurchesFinishedEvent(Map<String, Church> gotChurches) {
        mGotChurches = gotChurches;
    }

    public Map<String, Church> getChurches() {
        return mGotChurches;
    }

    public LoadChurchesFinishedMode getMode() {
        return mMode;
    }

    public void setMode(LoadChurchesFinishedMode mMode) {
        this.mMode = mMode;
    }

    public enum LoadChurchesFinishedMode {
        FULL,
        ADD
    }

}
