package com.massnow.app.logic.event;

import com.google.android.gms.maps.model.LatLng;

/**
 * FirstLocationResponseEvent
 *
 * @author Jakub Zitny <t102012001@ntut.edu.tw>
 * @since Jun 11 06:00 2014
 */
public class FirstLocationResponseEvent {

    private LatLng mUserLoc;

    public static FirstLocationResponseEvent create (LatLng userLoc) {
        FirstLocationResponseEvent firstLocationResponseEvent = new FirstLocationResponseEvent();
        firstLocationResponseEvent.setUserLoc(userLoc);
        return firstLocationResponseEvent;
    }

    public LatLng getUserLoc() {
        return mUserLoc;
    }

    public void setUserLoc(LatLng mUserLoc) {
        this.mUserLoc = mUserLoc;
    }
}
