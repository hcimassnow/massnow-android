package com.massnow.app.logic.event;

/**
 * LoadChurchesEvent
 *
 * @author Jakub Zitny <t102012001@ntut.edu.tw>
 * @since Jun 06 13:29 2014
 */
public class LoadChurchesEvent {

    private LoadChurchMode mMode;

    public LoadChurchMode getMode() {
        return mMode;
    }

    public void setMode(LoadChurchMode mode) {
        this.mMode = mode;
    }

    public static LoadChurchesEvent create() {
        return create(LoadChurchMode.INITIAL);
    }

    public static LoadChurchesEvent create(LoadChurchMode mode) {
        LoadChurchesEvent event = new LoadChurchesEvent();
        event.setMode(mode);
        return event;
    }

    private LoadChurchesEvent() {

    }

    public enum LoadChurchMode {
        INITIAL,
        FULL,
        SEARCH,
        CUSTOM
    }

}
