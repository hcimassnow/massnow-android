package com.massnow.app.logic.event;

import android.support.v4.app.Fragment;
import com.massnow.app.data.entity.Church;

/**
 * ReplaceChurchFragmentEvent
 * TODO: send church to controller, not activity
 * @author Jakub Zitny <t102012001@ntut.edu.tw>
 * @since Jun 11 05:06 2014
 */
public class ReplaceChurchFragmentEvent extends ReplaceFragmentEvent {

    private Church mChurch;

    /**
     * factory method
     * @param church
     * @return
     */
    public static ReplaceChurchFragmentEvent create (Church church) {
        ReplaceChurchFragmentEvent replaceChurchFragmentEvent = new ReplaceChurchFragmentEvent();
        replaceChurchFragmentEvent.setChurch(church);
        return replaceChurchFragmentEvent;
    }

    protected ReplaceChurchFragmentEvent () {

    }

    public Church getChurch() {
        return mChurch;
    }

    public void setChurch(Church mChurch) {
        this.mChurch = mChurch;
    }
}
