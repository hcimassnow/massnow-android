package com.massnow.app.logic.event;

import com.massnow.app.data.entity.Church;

/**
 * ReplaceChurchFragmentEvent
 * TODO: send church to controller, not activity
 * @author Jakub Zitny <t102012001@ntut.edu.tw>
 * @since Jun 11 05:06 2014
 */
public class ReplaceChurchRouteFragmentEvent extends ReplaceChurchFragmentEvent {

    /**
     * factory method
     * @param church
     * @return
     */
    public static ReplaceChurchRouteFragmentEvent create (Church church) {
        ReplaceChurchRouteFragmentEvent replaceChurchRouteFragmentEvent
                = new ReplaceChurchRouteFragmentEvent();
        replaceChurchRouteFragmentEvent.setChurch(church);
        return replaceChurchRouteFragmentEvent;
    }

}
