package com.massnow.app.logic;

import android.content.Context;
import android.util.Log;
import com.google.common.eventbus.Subscribe;
import com.massnow.app.Constants;
import com.massnow.app.data.entity.Church;
import com.massnow.app.data.entity.ChurchesCouchResponse;
import com.massnow.app.data.rest.ChurchRequest;
import com.massnow.app.data.rest.ChurchService;
import com.massnow.app.logic.event.*;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import de.greenrobot.event.EventBus;

import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * controller for church (result) collection
 * used for searched churches and for default closest churches screen
 * @author Jakub Zitny <t102012001@ntut.edu.tw>
 * @since Sun May 18 16:24:22 CST 2014
 */
@Singleton
public class ChurchListControl {

    public static final long CACHE_DURATION = DurationInMillis.ONE_HOUR;
    public static final String CACHE_KEY_PREFIX = "MN_CACHE_";

    private LocationControl mLocationControl;
    private SpiceManager mSpiceManager;

    private Map<String, Church> mChurches = new HashMap<String, Church>();
    private ChurchRequest mChurchRequest;
    private String mLastDocId = "";
    private String mCurrentCacheKey = CACHE_KEY_PREFIX;

    /************************************************
     ************************************************
     * OBJECT LIFECYCLE
     ************************************************
     ***********************************************/

    /**
     * factory method
     * @param context pass context to retrieve needed services
     * @return ChurchCollectionControl instance
     */
    public static ChurchListControl create(Context context) {
        return new ChurchListControl(context);
    }

    /**
     * prepare needed stuff
     * @param context pass context for services and get rid of it
     */
    public ChurchListControl(Context context) {
        EventBus.getDefault().register(this);
        mSpiceManager = new SpiceManager(ChurchService.class);
        mSpiceManager.start(context);
        mChurchRequest = ChurchRequest.create();
        mLocationControl = LocationControl.create(context);
    }

    /**
     * TODO: check event bus' unregisterment
     * "destructor" to be called from onstop
     */
    public void clean () {
        EventBus.getDefault().unregister(this);
        if (mSpiceManager.isStarted()) {
            mSpiceManager.shouldStop();
        }
    }

    /************************************************
     ************************************************
     * LOGIC
     ************************************************
     ***********************************************/

    /**
     * loads more churches
     */
    private void loadMore() {
        if (!mLastDocId.equals("")) {
            mChurchRequest.setStartKeyDocId(mLastDocId);
            mCurrentCacheKey = CACHE_KEY_PREFIX + mLastDocId;
        }
        mSpiceManager.execute(mChurchRequest, mCurrentCacheKey, CACHE_DURATION, new RestRequestListener());
    }

    /**
     * basic simple name-based search
     * @param query query to search in
     * @return map of hits
     */
    private Map<String, Church> search (String query) {
        Map<String, Church> hits = new HashMap<String, Church>();
        for (String name: mChurches.keySet()) {
            if (name.toLowerCase().contains(query.toLowerCase())) {
                hits.put(name, mChurches.get(name));
            }
        }
        return hits;
    }

    /**
     * mChurches getter
     * @return current mChurches here
     */
    public Map<String, Church> getChurches() {
        return mChurches;
    }

    /************************************************
     ************************************************
     * EVENTS
     ************************************************
     ***********************************************/

    @Subscribe
    public void onEvent(LoadChurchesEvent event) {
        Log.d("event", "loading more churches (" + mLastDocId + ")");
        loadMore();
    }

    /**
     * TODO: finish this and think
     * @param event with query user typed in
     */
    @Subscribe
    public void onEvent (SearchEvent event) {
        Map<String, Church> hits = search(event.getQuery());
        EventBus.getDefault().post(LoadChurchesFinishedEvent.create(hits,
                LoadChurchesFinishedEvent.LoadChurchesFinishedMode.FULL));
    }

    @Subscribe
    public void onEvent (ChurchMapRequestEvent event) {
        EventBus.getDefault().post(ChurchMapResponseEvent.create(mChurches));
    }

    /************************************************
     ************************************************
     * PRIVATE/MEMBER CLASSES
     ************************************************
     ***********************************************/

    /**
     * custom request listener handling finished rest requests
     * since requests are in other threads this is fired asynchronously
     * @author Jakub Zitny <t102012001@ntut.edu.tw>
     * @since Sun May 18 19:49:12 CST 2014
     */
    public final class RestRequestListener implements RequestListener<ChurchesCouchResponse> {

        @Override
        public void onRequestSuccess(ChurchesCouchResponse result) {
            if (Constants.DEBUG) Log.d("cb-succ: ", result.toString());
            List<Church> gotChurches = (ArrayList<Church>) result.getRows();
            // remove the one that'll be first in next iteration
            // so it's not repeaded
            // remove for debugging purposes
            if (gotChurches.size() > 0) {
                mLastDocId = gotChurches.get(gotChurches.size() - 1).getId();
                gotChurches.remove(gotChurches.size() - 1);
            } else {
                // TODO: block future requests
                mLastDocId = "";
            }
            if (Constants.DEBUG) {
                Log.d("updating id", mLastDocId.toString());
            }
            Map<String, Church> gotChurchesMap = new HashMap<String, Church>();
            for (Church church: gotChurches) {
                gotChurchesMap.put(church.getName(), church);
                mChurches.put(church.getName(), church);
            }
            // catch null
            EventBus.getDefault().post(LoadChurchesFinishedEvent.create(gotChurchesMap,
                    LoadChurchesFinishedEvent.LoadChurchesFinishedMode.ADD));
        }

        /**
         * TODO handle the exceptions better!
         * @param spiceException
         */
        @Override
        public void onRequestFailure(SpiceException spiceException) {
            //Toast.makeText(getActivity(), "Error: " + spiceException.getMessage(), Toast.LENGTH_SHORT).show();
            if (Constants.DEBUG) Log.e("cb-fail: ", spiceException.getMessage());
        }
    }

}
