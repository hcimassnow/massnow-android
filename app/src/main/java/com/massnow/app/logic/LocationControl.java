package com.massnow.app.logic;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.model.LatLng;
import com.google.common.eventbus.Subscribe;
import com.massnow.app.logic.event.FirstLocationRequestEvent;
import com.massnow.app.logic.event.FirstLocationResponseEvent;
import de.greenrobot.event.EventBus;

/**
 * LocationControl
 * TODO: logs
 * @author Jakub Zitny <t102012001@ntut.edu.tw>
 * @since Jun 10 16:47 2014
 */
public class LocationControl {

    private Location mCurrentBestEstimate = null;
    private boolean mGpsEnabled;
    private boolean mNetworkEnabled;
    private LocationManager mLocationManager;

    private boolean mGotFlre = false;

    /************************************************
     ************************************************
     * OBJECT LIFECYCLE
     ************************************************
     ***********************************************/

    /**
     * factory method
     * @param context pass context
     * @return return LocationControl instance
     */
    public static LocationControl create(Context context) {
        return new LocationControl(context);
    }

    /**
     * private controller
     * @param context pass context
     */
    private LocationControl(Context context) {
        mLocationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        start();
    }

    /**
     * "destructor" to unregister from bus
     */
    public void clean () {
        EventBus.getDefault().unregister(this);
    }

    /************************************************
     ************************************************
     * LOGIC
     ************************************************
     ***********************************************/

    /**
     * sets the basic needed vars
     * checks the last cached loc
     * dispatches the location logic to separate thread
     */
    public void start() {
        EventBus.getDefault().register(this);
        mGpsEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        mNetworkEnabled = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        Log.d("massnow", "gps(" + mGpsEnabled + "), net("+mNetworkEnabled+")");
        mCurrentBestEstimate = mLocationManager.getLastKnownLocation(getBestProvider());
        // TODO: dispatch following to background (or loading bar?)
        // long minTime (ms), float minDistance (m)
        mLocationManager.requestLocationUpdates(getBestProvider(), 10000, 50f, new CurrentLocationListener());

    }

    /**
     * TODO: better solution
     * @return string of the best location provider
     */
    private String getBestProvider() {
        Criteria criteria = new Criteria();
        return mLocationManager.getBestProvider(criteria, false);
    }

    /************************************************
     ************************************************
     * EVENTS
     ************************************************
     ***********************************************/

    /**
     * TODO: allow gps check that..
     * @param event
     */
    @Subscribe
    public void onEvent (FirstLocationRequestEvent event) {
        Log.d("massnow", "got flre");
        mGotFlre = true;
        if (mCurrentBestEstimate != null) {
            LatLng userLoc = new LatLng(mCurrentBestEstimate.getLatitude(), mCurrentBestEstimate.getLongitude());
            EventBus.getDefault().post(FirstLocationResponseEvent.create(userLoc));
        } else {
            Log.d("massnow", "flre not satissfied yet");
        }
    }

    /************************************************
     ************************************************
     * PRIVATE/MEMBER CLASSES
     ************************************************
     ***********************************************/

    private class CurrentLocationListener implements LocationListener {

        @Override
        public void onLocationChanged(Location location) {
            mCurrentBestEstimate = location;
            Log.d("massnow", "cbe: " + mCurrentBestEstimate.getLatitude() + "," + mCurrentBestEstimate.getLongitude());
            if (mGotFlre) {
                LatLng userLoc = new LatLng(mCurrentBestEstimate.getLatitude(), mCurrentBestEstimate.getLongitude());
                EventBus.getDefault().post(FirstLocationResponseEvent.create(userLoc));
            } else {
                Log.d("massnow", "flre problem");
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    }

}
