package com.massnow.app.view.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.massnow.app.R;
import com.massnow.app.data.entity.Church;
import com.massnow.app.data.map.DirectionsJSONParser;

import com.massnow.app.view.BaseFragment;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * fragment displaying search results
 * TODO make this work
 * @author Jakub Zitny <jakub.zitny@gmail.com>
 * @since Fri Apr 25 14:27:35 HKT 2014
 */
public class ChurchRouteFragment extends Fragment implements LocationListener {

    private Church mChurch;
    private SupportMapFragment mMapFragment;
    private GoogleMap mMap;
    private ImageView mMapButton;
	private TextView mTxtQuery;
    private LocationManager locationManager;
    private String provider;
    Context context;
    private LatLng mylocation;
    private LocationManager lms;
    private LatLng taipeiB = new LatLng(25.09008, 121.5498);
    @InjectView(R.id.church_title2) TextView mTitle;

    private void locationServiceInitial() {
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);	//取得系統定位服務
        Location location = lms.getLastKnownLocation(LocationManager.GPS_PROVIDER);	//使用GPS定位座標
        getLocation(location);
    }
    private void getLocation(Location location) {	//將定位資訊顯示在畫面中
        if(location != null) {


            Double longitude = location.getLongitude();	//取得經度
            Double latitude = location.getLatitude();	//取得緯度
            mylocation = new LatLng(longitude,latitude);
        }
        else {

        }
    }

    public void Util(Context context) {
        this.context = context;
    }
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // TODO Auto-generated method stub

    }

    public void onLocationChanged(Location location) {
        double lat =  location.getLatitude();
        double lng = location.getLongitude();

        mylocation = new LatLng(lat, lng);
        String url = getDirectionsUrl(mylocation, taipeiB);
        float[] distance = new float[1];
        Location.distanceBetween(mylocation.latitude, mylocation.longitude,
                taipeiB.latitude, taipeiB.longitude, distance);

        for (int i = 0; i < distance.length; i++) {
            Log.d("massnow", "distance"+i+"=" + String.valueOf(distance[i]));
        }

        Log.d("massnow", "----");
        Location myLocLoc = new Location("myloc");
        myLocLoc.setLatitude(mylocation.latitude);
        myLocLoc.setLongitude(mylocation.longitude);

        Location themLocLoc = new Location("themloc");
        themLocLoc.setLatitude(taipeiB.latitude);
        themLocLoc.setLongitude(taipeiB.longitude);

        double dist2 = myLocLoc.distanceTo(themLocLoc);
        Log.d("massnow", "distance22=" + dist2);

        DownloadTask downloadTask = new DownloadTask();

        // Start downloading json data from Google Directions API
        Log.d("massnow", "url info: "+url);
        downloadTask.execute(url);

    }

    @Override
    public void onProviderEnabled(String provider) {
    }
    @Override
    public void onProviderDisabled(String provider) {

    }

	/**
	 * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater,
	 * android.view.ViewGroup, android.os.Bundle)
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_map2, null);
        ButterKnife.inject(this, view);
        //mTitle.setText(mChurch.getName());

		// txtQuery = (TextView) findViewById(R.id.txtQuery);
		// handleIntent(getIntent());

        FragmentManager fm = getFragmentManager();
        //    mMap = ((SupportMapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
        if (fm == null) Log.d("massnow", "got fm null");
        mMapFragment = (SupportMapFragment) fm.findFragmentById(R.id.gmap2);

        if (mMapFragment == null) {
            mMapFragment = SupportMapFragment.newInstance();
            fm.beginTransaction().replace(R.id.map, mMapFragment).commit();
            Log.d("massnow", "got map null");
        }

        mMap = mMapFragment.getMap();

        mMap.setMyLocationEnabled(true);

        LocationManager service = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        boolean enabledGPS = service
                .isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean enabledWiFi = service
                .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        // Check if enabled and if not send user to the GSP settings
        // Better solution would be to display a dialog and suggesting to
        // go to the settings

        if (!enabledGPS) {

            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(intent);
        }
        /*
        if (!enabledWiFi) {

            Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
            startActivity(intent);
        }
         */
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        // Define the criteria how to select the locatioin provider -> use
        // default
        Criteria criteria = new Criteria();
        provider = locationManager.getBestProvider(criteria, false);
        Log.d("massnow", provider);
        //Location location = locationManager.getLastKnownLocation(provider);
        boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        Log.d("massnow", "gps: " + String.valueOf(isGPSEnabled));
        Log.d("massnow", "new: " + String.valueOf(isNetworkEnabled));

        Location location = locationManager.getLastKnownLocation(provider);



        // Initialize the location fields
        if (location != null) {
            Log.d("massnow", "Provider " + provider + " has been selected.");
            onLocationChanged(location);

        } else {
            Log.d("massnow", "got location null");
            //do something
        }

     //   LatLng taipei = new LatLng(25.09108, 121.5598);
      //  taipeiB = new LatLng(25.09008, 121.5498);




      //  String url = getDirectionsUrl2(mylocation, mChurch.getStreet().replace(" ","%"));
/*
        String url = getDirectionsUrl(mylocation, taipeiB);
        DownloadTask downloadTask = new DownloadTask();
        // Start downloading json data from Google Directions API
        Log.d("url info", url);
        downloadTask.execute(url);
*/
//        mMap = ((SupportMapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();


//        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mylocation, 13));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(taipeiB, 13));

        mMap.addMarker(new MarkerOptions()
                .title("asd")
                .snippet("The most populous city in Taiwan.")
                .position(taipeiB));

        /*mMap.addMarker(new MarkerOptions()
                .title("TaipeiB")
                .snippet("The most populous city in TaiwanB.")
                .position(taipeiB));
*/
        return view;
	}

    private String getDirectionsUrl(LatLng origin,LatLng dest){

        // Origin of route
        String str_origin = "origin="+origin.latitude+","+origin.longitude;

        // Destination of route
        String str_dest = "destination="+dest.latitude+","+dest.longitude;

        // Sensor enabled
        String sensor = "sensor=true";

        // Building the parameters to the web service
        String parameters = str_origin+"&"+str_dest+"&"+sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters;

        return url;
    }

    private String getDirectionsUrl2(LatLng origin,String dest){

        // Origin of route
        String str_origin = "origin="+origin.latitude+","+origin.longitude;

        // Destination of route
        String str_dest = "destination="+dest;

        // Sensor enabled
        String sensor = "sensor=true";

        // Building the parameters to the web service
        String parameters = str_origin+"&"+str_dest+"&"+sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters;

        return url;
    }
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while( ( line = br.readLine()) != null){
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        }catch(Exception e){
            Log.d("massnow", "Exception while downloading url " + e.toString());
        }finally{
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try{
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            }catch(Exception e){
                Log.d("massnow", "Background Task: "+ e.toString());
            }
            return data;
        }
        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }

        // Fetches data from url passed


        /** A class to parse the Google Places in JSON format */
        private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String,String>>> >{

            // Parsing the data in non-ui thread
            @Override
            protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

                JSONObject jObject;
                List<List<HashMap<String, String>>> routes = null;

                try{
                    jObject = new JSONObject(jsonData[0]);
                    DirectionsJSONParser parser = new DirectionsJSONParser();

                    // Starts parsing data
                    routes = parser.parse(jObject);

                }catch(Exception e){
                    e.printStackTrace();
                }
                return routes;
            }

            // Executes in UI thread, after the parsing process
            @Override
            protected void onPostExecute(List<List<HashMap<String, String>>> result) {
                ArrayList<LatLng> points = null;
                PolylineOptions lineOptions = null;
                MarkerOptions markerOptions = new MarkerOptions();

                if (result == null) return;

                // Traversing through all the routes
                for(int i=0;i<result.size();i++){
                    points = new ArrayList<LatLng>();
                    lineOptions = new PolylineOptions();

                    // Fetching i-th route
                    List<HashMap<String, String>> path = result.get(i);

                    // Fetching all the points in i-th route
                    for(int j=0;j<path.size();j++){
                        HashMap<String,String> point = path.get(j);

                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        LatLng position = new LatLng(lat, lng);

                        points.add(position);
                    }

                    // Adding all the points in the route to LineOptions
                    lineOptions.addAll(points);
                    lineOptions.width(5);
                    lineOptions.color(Color.GREEN);
                }

                // Drawing polyline in the Google Map for the i-th route
                mMap.addPolyline(lineOptions);
            }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        SupportMapFragment f = (SupportMapFragment) getFragmentManager()
                .findFragmentById(R.id.gmap2);
        if (f != null)
            getFragmentManager().beginTransaction().remove(f).commit();
    }

    /**
     * sets the church data for this fragment
     * @param church church data represented in this fragment
     */
    public void setChurch(Church church) {
        mChurch = church;
    }

}
