package com.massnow.app.view;

import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.massnow.app.R;

/**
 * Created by USER on 5/30/2014.
 * TODO display Hello page on first time only
 * fragments displaying the main page
 */
public class HelloFragment extends BaseFragment{

    private TextView email;
    private TextView city;
    private Button enter;

    public static final String firstLaunch = "firstLaunch";
    public static final String emailSetting = "emailSettings";
    public static final String locationSetting = "locationSettings";

    /**
     * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater,
     * android.view.ViewGroup, android.os.Bundle)
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_hellopage, null);
        getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
        email = (TextView) view.findViewById(R.id.stupEmail);
        city = (TextView) view.findViewById(R.id.stupLoc);

        return view;
    }

    /**
     * this worked as activity
     * @param intent intent from main activity
     * TODO set the email on setting
     */
    @SuppressWarnings("unused")
    private void handleIntent(Intent intent) {
        if (Intent.ACTION_ATTACH_DATA.equals(intent.getAction())) {

        }
    }

    public void addListenerOnButton(View view) {
        enter = (Button) view.findViewById(R.id.btnHelloEnter);

        enter.setOnClickListener(new View.OnClickListener() {

            //Run when button is clicked
            @Override
            public void onClick(View v) {

                StringBuffer result = new StringBuffer();
                result.append(email.getText()).append(" ").append(city.getText());

                Toast.makeText(getActivity().getApplicationContext(), result.toString(),
                        Toast.LENGTH_LONG).show();

                SharedPreferences shrdPref = getActivity().getPreferences(0);
                SharedPreferences.Editor editor = shrdPref.edit();

                editor.putString(emailSetting, email.getText().toString());
                editor.putString(locationSetting, city.getText().toString());
                editor.commit();
            }
        });

    }
}