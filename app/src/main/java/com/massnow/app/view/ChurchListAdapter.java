package com.massnow.app.view;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.massnow.app.R;
import com.massnow.app.data.entity.Church;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * custom adapter class for church list
 * class has to be public for butterknife
 * @author Jakub Zitny <t102012001@ntut.edu.tw>
 * @since Fri Apr 25 14:22:44 HKT 2014
 * TODO maybe reuse for database?
 * TODO finish generic view
 */
public class ChurchListAdapter extends ArrayAdapter<Church> {

    private LayoutInflater mInflater;
    private final Context mContext;
    private final List<Church> mChurches;

    /**
     * holds the view data of church entity
     * @author Jakub Zitny <jakub.zitny@gmail.com>
     * @since Fri Apr 25 14:23:48 HKT 2014
     */
    static class ViewHolder {
        @InjectView(R.id.item_title) TextView name;
        @InjectView(R.id.item_address) TextView address;
        @InjectView(R.id.item_distance) TextView distance;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }

    /**
     * constructor
     * @param context activity context
     * @param churches array of Church objects to display
     */
    public ChurchListAdapter(Context context, List<Church> churches) {
        super(context, 0, churches);
        mInflater = LayoutInflater.from(context);
        mChurches = churches;
        mContext = context;
    }

    /**
     * fills the view data
     * @see android.widget.ArrayAdapter#getView(int, android.view.View,
     * android.view.ViewGroup)
     */
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holder;
        if (view != null) {
            holder = (ViewHolder) view.getTag();
        } else {
            view = mInflater.inflate(R.layout.list_item, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }
        Church church = (Church) getItem(position);
        //holder.name.setText("9:30 " + church.getName());
        holder.name.setText(church.getName());
        holder.address.setText(church.getStreet());
        //holder.distance.setText("7 minutes walking - navigate");
        holder.distance.setText("");
        return view;
    }

    private final Object mLock = new Object();
    private List<Church> mObjects;
    private ArrayList<Church> mOriginalValues;

    /**
     * custom support addAll method
     * original addAll supports only API 11+
     * thx to http://git.io/OoIm3Q and http://goo.gl/lur64e
     * TODO: use this only where needed
     * @param collection churches to add
     */
    public void addAll(Collection<? extends Church> collection) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            super.addAll(collection);
        } else {
            // TODO: fix this
            synchronized (mLock) {
                clear();
                for (Church church : collection) {
                    add(church);
                }
            }
        notifyDataSetChanged();
        }
    }

}