package com.massnow.app.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import com.massnow.app.data.rest.ChurchService;
import com.octo.android.robospice.SpiceManager;
import de.greenrobot.event.EventBus;

/**
 * fragment extension with EventBus registration
 * RoboSpice lifecycle has been moved elsewhere
 * custom base class for other fragments
 * @author Jakub Zitny <t102012001@ntut.edu.tw>
 * @since Sun May 18 20:38:54 CST 2014
 */
public class BaseFragment extends Fragment {

    private static final String TITLE = "MassNow";

    /************************************************
     ************************************************
     * FRAGMENT LIFECYCLE
     ************************************************
     ***********************************************/

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        super.onResume();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(TITLE);
        // prefer the components setup in child, not here
    }

}
