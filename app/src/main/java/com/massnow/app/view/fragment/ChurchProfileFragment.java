package com.massnow.app.view.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.massnow.app.R;
import com.massnow.app.data.entity.Church;
import com.massnow.app.data.entity.MassInfo;
import com.massnow.app.logic.event.ReplaceChurchFragmentEvent;
import com.massnow.app.logic.event.ReplaceChurchRouteFragmentEvent;
import com.massnow.app.view.BaseFragment;
import de.greenrobot.event.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO: design!
 * fragment displaying church details
 * @author Jakub Zitny <jakub.zitny@gmail.com>
 * @since Sat Apr 26 03:10:01 HKT 2014
 */
public class ChurchProfileFragment extends Fragment {

    private static final String TITLE = "MassNow - Church";

	private Church mChurch;
	private SupportMapFragment mMapFragment;
	private GoogleMap mMap;

    @InjectView(R.id.church_title) TextView mTitle;
    @InjectView(R.id.church_address) TextView mAddress;
    @InjectView(R.id.button_star) ImageView mRouteButton;
    @InjectView(R.id.button_routemaps) ImageView mRouteMapsButton;

    @InjectView(R.id.monday_time) TextView mMondayTimeView;
    @InjectView(R.id.tuesday_time) TextView mTuesdayTimeView;
    @InjectView(R.id.wednesday_time) TextView mWednesdayTimeView;
    @InjectView(R.id.thursday_time) TextView mThursdayTimeView;
    @InjectView(R.id.friday_time) TextView mFridayTimeView;
    @InjectView(R.id.saturday_time) TextView mSaturdayTimeView;
    @InjectView(R.id.sunday_time) TextView mSundayTimeView;

    /**
	 * creates the view
	 * displays the church page details
	 * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater,
	 * android.view.ViewGroup, android.os.Bundle)
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_church, null);
        ButterKnife.inject(this, view);

        mTitle.setText(mChurch.getName());
        mAddress.setText(mChurch.getStreet());
        mRouteButton.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(ReplaceChurchRouteFragmentEvent.create(mChurch));
            }

        });
        mRouteMapsButton.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v) {
            String uri = "http://maps.google.com/maps?saddr&sensor=TRUE"+"&daddr="+mChurch.getLatLng().latitude+","+mChurch.getLatLng().longitude;
            Log.d("massnow", "mapurl: " + uri);
            Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
            intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
            startActivity(intent);
            }

        });

        List<List<MassInfo>> daySchedule = mChurch.getSchedule().getDays();

        List<TextView> dayView = new ArrayList<TextView>();
        dayView.add(mMondayTimeView);
        dayView.add(mTuesdayTimeView);
        dayView.add(mWednesdayTimeView);
        dayView.add(mThursdayTimeView);
        dayView.add(mFridayTimeView);
        dayView.add(mSaturdayTimeView);
        dayView.add(mSundayTimeView);
        try {
            for (int i = 1; i <= 7; i++ ) {
                List<MassInfo> day = daySchedule.get(i);
                if (day.size() > 0) {
                    String text = "";
                    for (MassInfo mi : day) {
                        text += mi.getTime() + " (" + mi.getLang() + ") ";
                    }
                    dayView.get(i-1).setText(text);
                } else {
                    dayView.get(i-1).setText("-");
                }
            }
        } catch (Exception e) {
            Log.d("massnow", "ex: " + e.getMessage());

        }

		return view;
	}

	/**
	 * retrieves map on resume
	 * @see android.support.v4.app.Fragment#onResume()
	 */
	@Override
	public void onResume() {
		super.onResume();
		/* TODO check this
        if (mMap == null) {
			mMap = mMapFragment.getMap();
			// mMap.addMarker(new MarkerOptions().position(new LatLng(0, 0)));
		}
		*/
	}

	/**
	 * sets the church data for this fragment
	 * @param church church data represented in this fragment
	 */
	public void setChurch(Church church) {
		mChurch = church;
	}

}
