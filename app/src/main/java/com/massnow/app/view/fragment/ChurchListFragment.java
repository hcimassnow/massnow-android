package com.massnow.app.view.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import android.widget.AdapterView.OnItemClickListener;

import butterknife.ButterKnife;
import butterknife.InjectView;
import com.google.common.eventbus.Subscribe;
import com.massnow.app.logic.event.ReplaceChurchFragmentEvent;
import com.massnow.app.view.BaseFragment;
import com.massnow.app.R;
import com.massnow.app.data.entity.Church;
import com.massnow.app.logic.event.LoadChurchesEvent;
import com.massnow.app.logic.event.LoadChurchesFinishedEvent;
import com.massnow.app.logic.event.ReplaceFragmentEvent;
import com.massnow.app.view.ChurchListAdapter;
import de.greenrobot.event.EventBus;

import java.util.ArrayList;

/**
 * default fragment for main activity
 * holds the retrieved list of "current" churches
 * @author Jakub Zitny <jakub.zitny@gmail.com>
 * @since Fri Apr 25 14:13:43 HKT 2014
 */
public class ChurchListFragment extends BaseFragment {

    private ChurchListAdapter mChurchListAdapter;

    @InjectView(R.id.list) ListView list;

    /************************************************
     ************************************************
     * FRAGMENT LIFECYCLE
     ************************************************
     ***********************************************/

    /**
     * TODO: robospice cache, plug listener to pending request!
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ArrayList<Church> dummy = new ArrayList<Church>();
        mChurchListAdapter = new ChurchListAdapter(getActivity(), dummy);
    }

    @Override
    public void onStart() {
        super.onStart();
        // request first churches
        EventBus.getDefault().post(LoadChurchesEvent.create());
    }

	/**
     * use the custom clickscrolllistener for listview
	 * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater,
	 * android.view.ViewGroup, android.os.Bundle)
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_list, null);
        ButterKnife.inject(this, view);
		list.setAdapter(mChurchListAdapter);
        ClickScrollListener cs = new ClickScrollListener();
		list.setOnItemClickListener(cs);
        list.setOnScrollListener(cs);
        // and reset the map/list button to original state
        getActivity().invalidateOptionsMenu();
		return view;
	}

    /**
     * catches event from controller that dataset is ready
     * ref at http://goo.gl/sqEWBl
     * TODO: make event modes (full, partial)
     * @param event event with data to update
     */
    @Subscribe
    public void onEvent(LoadChurchesFinishedEvent event) {
        if (event.getMode() == LoadChurchesFinishedEvent.LoadChurchesFinishedMode.FULL) {
            mChurchListAdapter.clear();
            mChurchListAdapter.addAll(event.getChurches().values());
        } else {
            mChurchListAdapter.addAll(event.getChurches().values());
            mChurchListAdapter.notifyDataSetChanged();
        }
    }

    /************************************************
     ************************************************
     * PRIVATE/MEMBER CLASSES
     ************************************************
     ***********************************************/

    /**
     * custom listener extension for fragment click and scroll actions
     * scroll listener ref at http://goo.gl/RMqDmy
     * @author Jakub Zitny <t102012001@ntut.edu.tw>
     * @since Sun May 18 19:55:10 CST 2014
     */
    private final class ClickScrollListener implements OnItemClickListener, AbsListView.OnScrollListener {

        private int counter = 0;
        private int priorFirst = -1;

        /**
         * handles the clicked church row
         * sends event to replace the list fragment with church profile fragment
         * TODO: move the church fragment creation elsewhere?
         * @see android.widget.AdapterView.OnItemClickListener#onItemClick(android.widget.AdapterView,
         * android.view.View, int, long)
         */
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
            EventBus.getDefault().post(ReplaceChurchFragmentEvent.create(
                    (Church) parent.getAdapter().getItem(pos)));
        }

        /**
         * handles the scrolling event
         * figures out when to load more churches from server
         * inspiration from http://goo.gl/OmVJHC
         * TODO: finish this
         */
        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            if (visibleItemCount < totalItemCount && firstVisibleItem + visibleItemCount == totalItemCount) {
                // load more
                if (firstVisibleItem != priorFirst) {
                    Log.d("loading-more", "-----");
                    priorFirst = firstVisibleItem;
                    EventBus.getDefault().post(LoadChurchesEvent.create(LoadChurchesEvent.LoadChurchMode.CUSTOM));
                }
            }
        }

        /**
         * thx to http://stackoverflow.com/a/9793569/1893452
         * @param view
         * @param scrollState if 0 it denotes stopped scrolling
         */
        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
            // Do nothing yet
        }

    }
}
