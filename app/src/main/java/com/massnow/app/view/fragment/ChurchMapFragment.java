package com.massnow.app.view.fragment;

import android.content.Context;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.common.eventbus.Subscribe;
import com.massnow.app.R;
import com.massnow.app.data.entity.Church;
import com.massnow.app.logic.event.*;
import com.massnow.app.view.BaseFragment;
import de.greenrobot.event.EventBus;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * fragment displaying search results
 * @author Jakub Zitny <jakub.zitny@gmail.com>
 * @since Fri Apr 25 14:27:35 HKT 2014
 */
public class ChurchMapFragment extends Fragment {

    private GoogleMap mMap;
    private MapView mMapView;

    /************************************************
     ************************************************
     * FRAGMENT LIFECYCLE
     ************************************************
     ***********************************************/

	/**
     * TODO: not enabled gps
	 * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater,
	 * android.view.ViewGroup, android.os.Bundle)
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_map, null);
        ButterKnife.inject(this, view);

        MapsInitializer.initialize(getActivity());
        mMapView = (MapView) view.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);
        setUpMapIfNeeded(view);

        // TODO: move to onresume and think about the lifecycle
        EventBus.getDefault().register(this);
        // TODO: prepare map earlier
        EventBus.getDefault().post(FirstLocationRequestEvent.create());
        EventBus.getDefault().post(ChurchMapRequestEvent.create());
        // request redraw menu icon
        EventBus.getDefault().post(RedrawMenuEvent.create(RedrawMenuEvent.RedrawMenuEventIcon.LIST));

        return view;
	}

    @Override
    public void onResume() {
        super.onResume();
        if(mMapView != null)
            mMapView.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(mMapView != null) mMapView.onDestroy();
    }

    @Override
    public void onPause() {
        if(mMapView != null) mMapView.onPause();
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if(mMapView != null)
            mMapView.onSaveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        if(mMapView != null)
            mMapView.onLowMemory();
        super.onLowMemory();
    }

    /************************************************
     ************************************************
     * LOGIC
     ************************************************
     ***********************************************/

    /**
     * thx to http://goo.gl/xtbGme
     * @param view
     */
    private void setUpMapIfNeeded(View view) {
        if (mMap == null) {
            mMap = ((MapView) view.findViewById(R.id.mapView)).getMap();
            if (mMap != null) {
                mMap.setMyLocationEnabled(true);
                //setUpMap();
            }
        } else {
            mMap = ((MapView) view.findViewById(R.id.mapView)).getMap();
            mMap.setMyLocationEnabled(true);
        }
    }

    /************************************************
     ************************************************
     * EVENTS
     ************************************************
     ***********************************************/

    /**
     * TODO: pass only coordinates
     * @param event
     */
    @Subscribe
    public void onEvent (ChurchMapResponseEvent event) {
        for (Church church: event.getChurches().values()) {
            LatLng churchLoc = church.getLatLng();
            //Log.d("massnow", "church: "+ churchLoc.latitude + ":" + churchLoc.longitude);
            mMap.addMarker(new MarkerOptions().position(churchLoc));
        }
    }

    @Subscribe
    public void onEvent (FirstLocationResponseEvent event) {
        Log.d("massnow", "got location map");
        LatLng userLoc = event.getUserLoc();
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(userLoc, 13));
        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @InjectView(R.id.timetable_content)
            TextView mTimeTableView;

            // Use default InfoWindow frame
            @Override
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            // Defines the contents of the InfoWindow
            @Override
            public View getInfoContents(Marker arg0) {
                // Getting view from the layout file info_window_layout
                View v = getActivity().getLayoutInflater().inflate(R.layout.fragment_googlemap, null);
                TextView mTitle = (TextView) v.findViewById(R.id.church_title);
                TextView mTimeTableView = (TextView) v.findViewById(R.id.timetable_content);
                //mTitle.setText(mChurch.getName());
                mTimeTableView.setText("Monday      10:00");
                return v;
            }
        });
    }

}

