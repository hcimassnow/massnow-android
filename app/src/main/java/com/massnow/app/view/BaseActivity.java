package com.massnow.app.view;

import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBarActivity;
import android.widget.Toast;
import com.google.common.eventbus.Subscribe;
import com.massnow.app.data.rest.ChurchService;
import com.massnow.app.logic.event.ToastEvent;
import com.octo.android.robospice.SpiceManager;
import de.greenrobot.event.EventBus;

/**
 * BaseActivity implements eventbus
 * and spice manager lifecycle management
 * TODO: rethink the spicemanager
 * @author Jakub Zitny <t102012001@ntut.edu.tw>
 * @since Jun 06 22:03 2014
 */
public class BaseActivity extends FragmentActivity {

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        super.onResume();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        //mSpiceManager.start(this);
    }

    /**
     * bit ugly because of http://goo.gl/Jg5dkN
     * makes shure spice manager stops
     */
    @Override
    public void onStop() {
//        if (mSpiceManager.isStarted()) {
//            mSpiceManager.shouldStop();
//        }
        super.onStop();
    }

//    protected SpiceManager getSpiceManager() {
//        return mSpiceManager;
//    }

    /**
     * universal toasting event for all sub activities
     * @param toastEvent event with toast message
     */
    @Subscribe
    public void onEvent (ToastEvent toastEvent) {
        Toast.makeText(this, toastEvent.getMessage(),
                Toast.LENGTH_LONG).show();
    }

}
