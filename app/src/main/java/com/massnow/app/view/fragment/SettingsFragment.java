package com.massnow.app.view.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Toast;

import com.massnow.app.R;

/**
 * fragment for settings
 * TODO PreferenceFragment with support http://goo.gl/WgMjGM
 * TODO design
 * TODO inspire at example PreferenceActivity
 * @author Jakub Zitny <jakub.zitny@gmail.com>
 * @since Fri Apr 25 14:31:13 HKT 2014
 */
public class SettingsFragment extends Fragment {

    private static final String TITLE = "MassNow - Settings";

	/**
	 * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater,
	 * android.view.ViewGroup, android.os.Bundle)
	 */
    private static CheckBox cbOffline;
    private static CheckBox cbFavourites;

    SharedPreferences settings;
    SharedPreferences.Editor editor;

    public static final String favPref = "favouriteMode";
    public static final String offPref = "offlineMode";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_settings, null);
		getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);

        Log.i("SettingPage :", "getSharedPreferences");
        settings = getActivity().getPreferences(0);
        editor = settings.edit();
        cbOffline = (CheckBox) view.findViewById(R.id.cbSettingOffline);
        cbFavourites = (CheckBox) view.findViewById(R.id.cbSettingFav);

        cbOffline.setChecked(settings.getBoolean(offPref, false));
        cbFavourites.setChecked(settings.getBoolean(favPref, false));

        //onCheckboxClicked(view);

//        cbOffline.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (((CheckBox) view).isChecked()) {
//                    Toast.makeText(getActivity().getApplicationContext(), "Checked", Toast.LENGTH_SHORT).show();
//                } else {
//                    Toast.makeText(getActivity().getApplicationContext(), "Unchecked", Toast.LENGTH_SHORT).show();
//                }
//                settingEditor.putBoolean("offlineMode", cbOffline.isChecked());
//                Log.i("CbOff : ", "committed");
//                settingEditor.commit();
//            }
//        });

        return view;
	}

//    public void onCheckboxClicked(View view){
//        // Is the view now checked?
//        boolean checked = ((CheckBox) view).isChecked();
//
//        // Check which checkbox was clicked
//        switch(view.getId()) {
//            case R.id.cbSettingOffline:
//                if (checked) {
//                    // Put some meat on the sandwich
//                    Toast.makeText(getActivity().getApplicationContext(), "Off Ch", Toast.LENGTH_SHORT).show();
//                } else {
//                    Toast.makeText(getActivity().getApplicationContext(), "Off Unch", Toast.LENGTH_SHORT).show();
//                    // Remove the meat
//                }
//                editor.putBoolean(offPref, cbOffline.isChecked());
//                break;
//            case R.id.cbSettingFav:
//                if (checked) {
//                    // Cheese me
//                    Toast.makeText(getActivity().getApplicationContext(), "Fav Ch", Toast.LENGTH_SHORT).show();
//                } else {
//                    // I'm lactose intolerant
//                    Toast.makeText(getActivity().getApplicationContext(), "Fav Unch", Toast.LENGTH_SHORT).show();
//                }
//                editor.putBoolean(favPref, cbFavourites.isChecked());
//                break;
//        }
//    }

    public void onDestroyView(){
        super.onDestroyView();

        Log.i("Setting onStop : ", "start Offline");
        editor.putBoolean(offPref, cbOffline.isChecked());
        Log.i("Setting onStop : ", "start Favourites");
        editor.putBoolean(favPref, cbFavourites.isChecked());
        Log.i("Setting onStop : ", "Commit");
        editor.commit();
    }
}
