package com.massnow.app.view;

import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.ComponentName;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager.OnBackStackChangedListener;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Menu;
import android.widget.SearchView;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.common.eventbus.Subscribe;
import com.massnow.app.R;
import com.massnow.app.logic.event.*;
import com.massnow.app.view.fragment.*;
import de.greenrobot.event.EventBus;
import org.apache.commons.lang3.NotImplementedException;

/**
 * main activity holds action bar (hopefully) dispatches all fragments
 * TODO: "onresume" display last fragment from backstack
 * TODO: does private fragmenttransaction make sense?
 * @author Jakub Zitny <jakub.zitny@gmail.com>
 * @since Fri Apr 25 13:39:12 HKT 2014
 */
public class MainActivity extends BaseActivity implements OnBackStackChangedListener {

    private FragmentTransaction mFragmentTransaction;
    private ChurchListFragment mChurchListFragment;
    private SettingsFragment mSettingsFragment;
    private ChurchMapFragment mChurchMapFragment;

    private Menu mMenu;

    public static final String firstLaunch = "firstLaunch";
    /**
     * sets up the up button functionality fills the default fragmentx
     * @see android.support.v4.app.FragmentActivity#onCreate(android.os.Bundle)
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // setup the up button
        shouldDisplayHomeUp();
        getSupportFragmentManager().addOnBackStackChangedListener(this);
        // fill the default fragment
        mFragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (mChurchListFragment == null) {
            mChurchListFragment = new ChurchListFragment();
        }

        SharedPreferences shrdPref = getPreferences(0);
        SharedPreferences.Editor editor = shrdPref.edit();
        // TODO: skip for now
        if (false && shrdPref.getBoolean(firstLaunch, true)){
            Toast.makeText(this, "first launch true", Toast.LENGTH_SHORT).show();
            HelloFragment helloPageFragment = new HelloFragment();
            Log.i("main", "first launch");
            mFragmentTransaction.replace(R.id.fragmentContainer, helloPageFragment);
            mFragmentTransaction.commit();

            Log.i("main", "change first launch to false");
            editor.putBoolean(firstLaunch, false);
            editor.commit();
        } else {
            //Toast.makeText(this, "first launch false", Toast.LENGTH_SHORT).show();
            mFragmentTransaction.replace(R.id.fragmentContainer, mChurchListFragment);
            mFragmentTransaction.commit();
        }
    }

    /**
     * TODO: handle case when searched from prefs (switch frag)
     * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        mMenu = menu;

        // TODO: expanding/collapsing http://goo.gl/BmlaTb ?
        MenuItem searchMenuItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchMenuItem.getActionView();
        // suport thx to http://goo.gl/5Yv64Z
        //SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchMenuItem);
        if (searchView == null) {
            Log.d("searchview compat", searchView.toString());
            //TODO: throw new Exception("asd");
        }
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        if (null != searchManager) {
            ComponentName componentName = getComponentName();
            SearchableInfo searchableInfo = searchManager.getSearchableInfo(componentName);
            searchView.setSearchableInfo(searchableInfo);
        }
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                EventBus.getDefault().post(SearchEvent.create(query));
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                // TODO: not implemented yet
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * @param fragment
     */
    public void replaceFragment (Fragment fragment) {
        mFragmentTransaction = getSupportFragmentManager().beginTransaction();
        mFragmentTransaction.replace(R.id.fragmentContainer, fragment);
        mFragmentTransaction.addToBackStack(null);
        mFragmentTransaction.commit();
    }

    @Subscribe
    public void onEvent (ReplaceFragmentEvent event) {
        if (event instanceof ReplaceChurchFragmentEvent) {
            ChurchProfileFragment churchProfileFragment = new ChurchProfileFragment();
            churchProfileFragment.setChurch(((ReplaceChurchFragmentEvent) event).getChurch());
            replaceFragment(churchProfileFragment);
        } else if (event instanceof ReplaceChurchRouteFragmentEvent) {
            ChurchRouteFragment churchRouteFragment = new ChurchRouteFragment();
            churchRouteFragment.setChurch(((ReplaceChurchRouteFragmentEvent) event).getChurch());
            replaceFragment(churchRouteFragment);
        } else {
            throw new NotImplementedException("not implemented yet");
            //TODO: replaceFragment(event.getFragment());
        }
    }

    @Subscribe
    public void onEvent (RedrawMenuEvent event) {
        if (event.getIcon() == RedrawMenuEvent.RedrawMenuEventIcon.LIST) {
            MenuItem actionMap = mMenu.findItem(R.id.action_map);
            actionMap.setIcon(getResources().getDrawable(R.drawable.ic_action_view_as_list));
            actionMap.setTitle("list");
        } else if (event.getIcon() == RedrawMenuEvent.RedrawMenuEventIcon.MAP) {
            MenuItem actionMap = mMenu.findItem(R.id.action_map);
            actionMap.setIcon(getResources().getDrawable(R.drawable.ic_action_map_light));
            actionMap.setTitle("map");
        }
    }

    /**
     * TODO: addtobackstack check?
     * dispatches actionbar actions commits corresponding fragments
     * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                // legacy ha
//                mFragmentTransaction = getSupportFragmentManager().beginTransaction();
//                if (mSearchFragment == null) {
//                    mSearchFragment = new SearchFragment();
//                }
//                mFragmentTransaction.replace(R.id.fragmentContainer, mSearchFragment);
//                mFragmentTransaction.addToBackStack(null);
//                mFragmentTransaction.commit();
//                break;
            case R.id.action_map:
                // TODO: really?
                if (item.getTitle().equals("map")) {
                    if (mChurchMapFragment == null) {
                        mChurchMapFragment = new ChurchMapFragment();
                    }
                    replaceFragment(mChurchMapFragment);
                } else if (item.getTitle().equals("list")) {
                    replaceFragment(mChurchListFragment);
                }
                break;
            case R.id.action_settings:
                if (mSettingsFragment == null) {
                    mSettingsFragment = new SettingsFragment();
                }
                replaceFragment(mSettingsFragment);
                break;
            case android.R.id.home:
                getActionBar().setDisplayHomeAsUpEnabled(false);
                if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    getSupportFragmentManager().popBackStack();
                }
                break;
            default:
                break;
        }
        return true;
    }



    /**
     * handle fragment backstack change
     * @see android.support.v4.app.FragmentManager.OnBackStackChangedListener#onBackStackChanged()
     */
    @Override
    public void onBackStackChanged() {
        shouldDisplayHomeUp();
    }

    /**
     * display home up button
     * only when there are entries in the back stack
     */
    public void shouldDisplayHomeUp() {
        boolean canback = getSupportFragmentManager().getBackStackEntryCount() > 0;
        getActionBar().setDisplayHomeAsUpEnabled(canback);
    }

    /**
     * called when up button is pressed
     * just pop the back stack
     * @see android.app.Activity#onNavigateUp()
     */
    @Override
    public boolean onNavigateUp() {
        getSupportFragmentManager().popBackStack();
        return true;
    }

}