package com.massnow.app.data.rest;

import com.massnow.app.Constants;
import com.massnow.app.data.entity.AllDocsCouchResponse;
import com.massnow.app.data.entity.Church;
import com.massnow.app.data.entity.ChurchesCouchResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import roboguice.util.temp.Ln;

/**
 * performs the request to retrieve some churches
 * @author Jakub Zitny <t102012001@ntut.edu.tw>
 * @since Sun May 18 19:17:01 CST 2014
 */
public class ChurchRequest extends RetrofitSpiceRequest<ChurchesCouchResponse, ChurchServiceInterface> {

    /**
     * the default limit for first request
     * at first request only 10 items and load others after scrolling
     * TODO: dynamically adjust this based on the device size
     */
    private final static int FIRST_LIMIT = 15;

    private String mIncludeDocs;
    private String mLimit;
    private String mStartKeyDocId;

    /**
     * factory method creating this request
     * @param limit number of churches to retrieve
     * @param startKeyDocId docId of first church where to start retrieving
     * @return created ChurchRequest
     */
    public static ChurchRequest create(int limit, String startKeyDocId) {
        return new ChurchRequest(limit, startKeyDocId);
    }

    /**
     * startkeydocid setter
     * @param startKeyDocId to start from on next request
     */
    public void setStartKeyDocId(String startKeyDocId) {
        mStartKeyDocId = startKeyDocId;
    }

    /**
     * factory method for starting without specifying first church
     * starts from the beginning of the database
     * @return created ChurchRequest
     */
    public static ChurchRequest create() {
        return create(FIRST_LIMIT, "");
    }

    /**
     * private constructor
     * @param limit number of churches to retrieve
     * @param startKeyDocId docId of first church where to start retrieving
     */
    private ChurchRequest(int limit, String startKeyDocId) {
        super(ChurchesCouchResponse.class, ChurchServiceInterface.class);
        mIncludeDocs = "true";
        mLimit = Integer.toString(limit);
        mStartKeyDocId = startKeyDocId;
    }

    /**
     * loads data after performing the actual request
     * @return retrieved AllDocs
     */
    @Override
    public ChurchesCouchResponse loadDataFromNetwork() {
        if (Constants.DEBUG) Ln.d("Call web service ");
        return getService().getChurches(mIncludeDocs, mLimit, mStartKeyDocId);
    }

}