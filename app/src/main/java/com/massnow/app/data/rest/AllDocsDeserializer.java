package com.massnow.app.data.rest;

import com.google.gson.*;
import com.massnow.app.data.entity.AllDocsCouchResponse;
import com.massnow.app.data.entity.Church;
import com.massnow.app.data.entity.ChurchesCouchResponse;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * implements deserialozation of json response from _all_docs couchdb
 * @author Jakub Zitny <t102012001@ntut.edu.tw>
 * @since Fri May 16 14:06:40 CST 2014
 */
public class AllDocsDeserializer implements JsonDeserializer<ChurchesCouchResponse> {

    /** TODO implement this for subclass */
    private static final boolean IS_DUMMY = false;

    @Override
    public ChurchesCouchResponse deserialize(final JsonElement json, final Type typeOfT,
                              final JsonDeserializationContext context) throws JsonParseException {

        final JsonObject jsonObject = json.getAsJsonObject();

        JsonElement totalRowsJson = jsonObject.get("total_rows");
        int totalRows = totalRowsJson.getAsInt();
        JsonElement offsetJson = jsonObject.get("offset");
        int offset = offsetJson.getAsInt();
        JsonArray rowsJson = jsonObject.get("rows").getAsJsonArray();
        List<Church> churches = new ArrayList<Church>();
        for (int i = 0; i < rowsJson.size(); i++) {
            JsonObject rowJson = rowsJson.get(i).getAsJsonObject();
            JsonElement idJson = rowJson.get("id");
            String id = (idJson.isJsonNull()) ? null : idJson.getAsString();
            JsonElement keyJson = rowJson.get("key");
            String key = (idJson.isJsonNull()) ? null : keyJson.getAsString();
            JsonElement docJson = rowJson.get("doc");
            Church church = context.deserialize(docJson, Church.class);
            churches.add(church);
        }
        return ChurchesCouchResponse.create(totalRows, offset, churches);
    }

}
