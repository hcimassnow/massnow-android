package com.massnow.app.data.entity;

/**
 * Created by d_rc on 5/16/14.
 */
public class MassInfo {

    private String mTime;
    private String mLang;

    public MassInfo(String time, String lang) {
        mTime = time;
        mLang = lang;
    }

    public String getTime() {
        return mTime;
    }

    public void setTime(String mTime) {
        this.mTime = mTime;
    }

    public String getLang() {
        return mLang;
    }

    public void setLang(String mLang) {
        this.mLang = mLang;
    }
}
