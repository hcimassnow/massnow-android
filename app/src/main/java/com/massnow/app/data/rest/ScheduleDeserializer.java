package com.massnow.app.data.rest;

import com.google.gson.*;
import com.massnow.app.data.entity.MassInfo;
import com.massnow.app.data.entity.Schedule;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * deserializes the schedule json from couchdb's docs
 * @author Jakub Zitny <t102012001@ntut.edu.tw>
 * @since Fri May 16 11:00:11 CST 2014
 */
public class ScheduleDeserializer implements JsonDeserializer {

    @Override
    public Schedule deserialize(final JsonElement json, final Type typeOfT,
            final JsonDeserializationContext context) throws JsonParseException {
        final JsonArray jsonArray = json.getAsJsonArray();

        List<List<MassInfo>> week = new ArrayList<List<MassInfo>>();
        for (int i = 0; i < jsonArray.size(); i++) {
            JsonElement paramJson = jsonArray.get(i);
            List<MassInfo> day = new ArrayList<MassInfo>();
            if (paramJson.isJsonNull()) {
                week.add(day);
                continue;
            }
            JsonArray dayJson = paramJson.getAsJsonArray();
            for (int j = 0; j < dayJson.size(); j++) {
                JsonObject massJson = dayJson.get(j).getAsJsonObject();
                JsonElement timeJson = massJson.get("time");
                String time = (timeJson.isJsonNull()) ? null : timeJson.getAsString();
                JsonElement langJson = massJson.get("lang");
                String lang = (langJson.isJsonNull()) ? null : langJson.getAsString();
                MassInfo mi = new MassInfo(time, lang);
                day.add(mi);
            }
            week.add(day);
        }

        final Schedule schedule = Schedule.create(week);
        return schedule;
    }

}
