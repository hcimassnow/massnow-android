package com.massnow.app.data.map;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.widget.TextView;

/**
 * Created by ggban on 5/23/14.
 */
public class MyCurrentLoctionListener implements LocationListener {

    public String myLocation;
    private TextView mTextView;

    MyCurrentLoctionListener(TextView tv) {
        this.mTextView = tv;
    }

    @Override
    public void onLocationChanged(Location location) {
        location.getLatitude();
        location.getLongitude();

        mTextView.setText("Latitude = " + location.getLatitude() + " Longitude = " + location.getLongitude());

    }
    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}
