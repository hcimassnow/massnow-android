package com.massnow.app.data.entity;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * church entity holds information about a church
 * TODO street string to google maps objects
 * @author Jakub Zitny <jakub.zitny@gmail.com>
 * @since Sat Apr 26 03:17:48 HKT 2014
 */
public class Church {

    private String mId;
    private String mRev;

	private String mName;
	private String mShortName;
    private String mStreet;
    private String mCity;
	private List<String> mPhones;
	private String mWeb;
	private String mEmail;
	private Schedule mSchedule;
    private LatLng mLatLng;

	/**
	 * empty constructor
	 */
	private Church() {

	}

    /**
     * debugging simple constructor
     * @param name
     */
    public Church(String name) {
        mName = name;
    }

	/**
	 * factory method
	 * @param name
	 * @param shortName
	 * @param street
	 * @param phones
	 * @param web
	 * @param email
	 * @param schedule
	 */
	public static Church create(String id, String rev, String name, String shortName, String street, String city,
            List<String> phones, String web, String email, Schedule schedule, LatLng latLng) {
        Church church = new Church();
		church.setId(id);
        church.setRev(rev);
        church.setName(name);
        church.setShortName(shortName);
        church.setStreet(street);
        church.setCity(city);
        church.setPhones(phones);
        church.setWeb(web);
        church.setEmail(email);
        church.setSchedule(schedule);
        church.setLatLng(latLng);
        return church;
	}

	/**
	 * @return the mName
	 */
	public String getName() {
		return mName;
	}

	/**
	 * @param mName the mName to set
	 */
	public void setName(String mName) {
		this.mName = mName;
	}

	/**
	 * @return the mShortName
	 */
	public String getShortName() {
		return mShortName;
	}

	/**
	 * @param mShortName the mShortName to set
	 */
	public void setShortName(String mShortName) {
		this.mShortName = mShortName;
	}

	/**
	 * @return the mStreet
	 */
	public String getStreet() {
		return mStreet;
	}

	/**
	 * @param mStreet the mStreet to set
	 */
	public void setStreet(String mStreet) {
		this.mStreet = mStreet;
	}

	/**
	 * @return the mPhones
	 */
	public List<String> getPhones() {
		return mPhones;
	}

	/**
	 * @param mPhones the mPhones to set
	 */
	public void setPhones(List<String> mPhones) {
		this.mPhones = mPhones;
	}

	/**
	 * @return the mWeb
	 */
	public String getWeb() {
		return mWeb;
	}

	/**
	 * @param mWeb the mWeb to set
	 */
	public void setWeb(String mWeb) {
		this.mWeb = mWeb;
	}

	/**
	 * @return the mEmail
	 */
	public String getEmail() {
		return mEmail;
	}

	/**
	 * @param mEmail the mEmail to set
	 */
	public void setEmail(String mEmail) {
		this.mEmail = mEmail;
	}

	/**
	 * @return the mSchedule
	 */
	public Schedule getSchedule() {
		return mSchedule;
	}

	/**
	 * @param mSchedule the mSchedule to set
	 */
	public void setSchedule(Schedule mSchedule) {
		this.mSchedule = mSchedule;
	}

    /**
     * Gets mKey.
     *
     * @return Value of mKey.
     */
    public String getRev() {
        return mRev;
    }

    /**
     * Sets new mId.
     *
     * @param mId New value of mId.
     */
    public void setId(String mId) {
        this.mId = mId;
    }

    /**
     * Gets mId.
     *
     * @return Value of id.
     */
    public String getId() {
        return mId;
    }

    /**
     * Sets new mKey.
     *
     * @param rev New value of rev.
     */
    public void setRev(String rev) {
        this.mRev = rev;
    }

    /**
     * Sets new mCity.
     *
     * @param mCity New value of mCity.
     */
    public void setCity(String mCity) {
        this.mCity = mCity;
    }

    /**
     * Gets mCity.
     *
     * @return Value of mCity.
     */
    public String getCity() {
        return mCity;
    }

    public LatLng getLatLng() {
        return mLatLng;
    }

    public void setLatLng(LatLng latLng) {
        this.mLatLng = latLng;
    }
}
