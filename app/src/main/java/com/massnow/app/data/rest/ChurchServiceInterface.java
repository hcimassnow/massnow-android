package com.massnow.app.data.rest;

import com.massnow.app.data.entity.AllDocsCouchResponse;
import com.massnow.app.data.entity.Church;
import com.massnow.app.data.entity.ChurchesCouchResponse;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * TODO: add limit option to list
 * TODO: maybe better name for this?
 * cdb _all_docs params ref at http://bit.ly/1n311Bi
 * @author Jakub Zitny <t102012001@ntut.edu.tw>
 * @since Sun May 18 20:56:46 CST 2014
 */
public interface ChurchServiceInterface {

    /**
     * the main request to get all documents from couchdb
     * @param includeDocs whether include whole docs in response (_all_docs default is false)
     * @param startKeyDocId first document to retrieve or empty string to start at beginning
     * @param limit number of docs to retrieve starting from startKeyDocId
     * @return retrieved alldocscouchresponse full of churches
     */
    @GET("/massnow_t/_all_docs")
    ChurchesCouchResponse getChurches(@Query("include_docs") String includeDocs,
          @Query("limit") String limit, @Query("startkey_docid") String startKeyDocId);


//    @GET("/massnow_t/_all_docs?include_docs=true")
//    void getChurchesCb(Callback<AllDocsCouchResponse> cb);
//
//    @GET("/massnow_t/_all_docs?include_docs=false")
//    void listChurches(Callback<AllDocsCouchResponse> cb);
//
//    @GET("/massnow_t/{churchId}")
//    void getChurch(@Path("churchId") String churchId, Callback<Church> cb);

}