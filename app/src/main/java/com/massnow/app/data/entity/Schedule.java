package com.massnow.app.data.entity;

import java.util.List;

/**
 * Created by d_rc on 5/16/14.
 */
public class Schedule {

    private List<List<MassInfo>> mDays;

    public class MassTime {
    }

    private Schedule() {
    }

    public void setDays(List<List<MassInfo>> days) {
        mDays = days;
    }

    public List<List<MassInfo>> getDays() {return mDays; }

    public static Schedule create(List<List<MassInfo>> week) {
        Schedule schedule = new Schedule();
        schedule.setDays(week);
        return schedule;
    }

}
