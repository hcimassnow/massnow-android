package com.massnow.app.data.entity;

import java.util.List;

/**
 * THIS IS DEPRECATED!
 * generic entity for _all_docs couchdb response
 * _all_docs returns list of all docs in a database
 * if run with include_docs=true it returns whole documents
 * if not it gives just keys, revs and ids to retrieve docs separately
 * @author Jakub Zitny <t102012001@ntut.edu.tw>
 * @since Fri May 16 14:06:40 CST 2014
 */
public class AllDocsCouchResponse<T> {

    protected int mTotalRows;
    protected int mOffset;
    protected List<T> mRows;

    /**
     * factory method
     * @param totalRows
     * @param offset
     * @param rows
     * @param <T>
     * @return
     */
    public static <T> AllDocsCouchResponse<T> createSuper(int totalRows, int offset, List<T> rows) {
        AllDocsCouchResponse<T> r = new AllDocsCouchResponse<T>();
        r.setOffset(offset);
        r.setTotalRows(totalRows);
        r.setRows(rows);
        return r;
    }

    /**
     * Gets mRows.
     *
     * @return Value of mRows.
     */
    public List<T> getRows() {
        return mRows;
    }

    /**
     * Sets new mOffset.
     *
     * @param mOffset New value of mOffset.
     */
    public void setOffset(int mOffset) {
        this.mOffset = mOffset;
    }

    /**
     * Gets mTotalRows.
     *
     * @return Value of mTotalRows.
     */
    public int getTotalRows() {
        return mTotalRows;
    }

    /**
     * Sets new mRows.
     *
     * @param mRows New value of mRows.
     */
    public void setRows(List<T> mRows) {
        this.mRows = mRows;
    }

    /**
     * Gets mOffset.
     *
     * @return Value of mOffset.
     */
    public int getOffset() {
        return mOffset;
    }

    /**
     * Sets new mTotalRows.
     *
     * @param mTotalRows New value of mTotalRows.
     */
    public void setTotalRows(int mTotalRows) {
        this.mTotalRows = mTotalRows;
    }
}
