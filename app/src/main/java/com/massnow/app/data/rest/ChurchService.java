package com.massnow.app.data.rest;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.massnow.app.data.entity.Church;
import com.massnow.app.data.entity.ChurchesCouchResponse;
import com.massnow.app.data.entity.Schedule;
import retrofit.RestAdapter;
import retrofit.RestAdapter.Builder;
import com.octo.android.robospice.retrofit.RetrofitGsonSpiceService;
import retrofit.converter.Converter;
import retrofit.converter.GsonConverter;

/**
 * retrofit church service for rest api
 * TODO: better name for this
 * refer http://bit.ly/1gCbjtU and http://bit.ly/1jUXzKc and http://bit.ly/1jUXzKc
 * @author Jakub Zitny <t102012001@ntut.edu.tw>
 * @since Sun May 18 20:56:46 CST 2014
 */
public class ChurchService extends RetrofitGsonSpiceService {

    private static final String API_URL = "http://178.77.239.211:5984/";
    private Converter converter;
    private Gson gsonBuilder;

    public ChurchService() {
        gsonBuilder = new GsonBuilder()
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .registerTypeAdapter(Church.class, new ChurchDeserializer())
            .registerTypeAdapter(Schedule.class, new ScheduleDeserializer())
            .registerTypeAdapter(ChurchesCouchResponse.class, new AllDocsDeserializer())
            .create();
        converter = new GsonConverter(gsonBuilder);
    }

    /**
     * refer http://bit.ly/1gCbjtU
     * @return
     */
    @Override
    protected Builder createRestAdapterBuilder() {
        RestAdapter.Builder restAdapter = super.createRestAdapterBuilder();
        restAdapter.setConverter(converter);
        restAdapter.setEndpoint(API_URL);
        restAdapter.build();
        return restAdapter;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        addRetrofitInterface(ChurchServiceInterface.class);
    }

    @Override
    protected String getServerUrl() {
        return API_URL;
    }

}