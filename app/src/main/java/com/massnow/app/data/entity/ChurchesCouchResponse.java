package com.massnow.app.data.entity;

import java.util.List;

/**
 * ChurchesCouchResponse
 *
 * @author Jakub Zitny <t102012001@ntut.edu.tw>
 * @since May 19 7:15 AM 2014
 */
public class ChurchesCouchResponse {

    protected int mTotalRows;
    protected int mOffset;
    protected List<Church> mRows;

    public static ChurchesCouchResponse create(int totalRows, int offset, List<Church> rows) {
        ChurchesCouchResponse r = new ChurchesCouchResponse();
        r.setOffset(offset);
        r.setTotalRows(totalRows);
        r.setRows(rows);
        return r;
    }

    /**
     * Sets new mOffset.
     *
     * @param mOffset New value of mOffset.
     */
    public void setOffset(int mOffset) {
        this.mOffset = mOffset;
    }

    /**
     * Gets mTotalRows.
     *
     * @return Value of mTotalRows.
     */
    public int getTotalRows() {
        return mTotalRows;
    }

    /**
     * Gets mRows.
     *
     * @return Value of mRows.
     */
    public List<Church> getRows() {
        return mRows;
    }

    /**
     * Sets new mTotalRows.
     *
     * @param mTotalRows New value of mTotalRows.
     */
    public void setTotalRows(int mTotalRows) {
        this.mTotalRows = mTotalRows;
    }

    /**
     * Sets new mRows.
     *
     * @param mRows New value of mRows.
     */
    public void setRows(List<Church> mRows) {
        this.mRows = mRows;
    }

    /**
     * Gets mOffset.
     *
     * @return Value of mOffset.
     */
    public int getOffset() {
        return mOffset;
    }
}
