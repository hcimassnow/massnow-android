package com.massnow.app.data.rest;

import com.massnow.app.data.rest.AllDocsDeserializer;

/**
 * TODO simple version for dummy _all_dos retrieval
 * implements deserialozation of json response from _all_docs couchdb
 * @author Jakub Zitny <t102012001@ntut.edu.tw>
 * @since Fri May 16 14:06:40 CST 2014
 */
public class AllDocsDummyDeserializer extends AllDocsDeserializer {

    private static final boolean IS_DUMMY = true;

}
