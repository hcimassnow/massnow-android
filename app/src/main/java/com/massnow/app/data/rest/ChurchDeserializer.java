package com.massnow.app.data.rest;

import android.util.Log;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.*;
import com.massnow.app.data.entity.Church;
import com.massnow.app.data.entity.Schedule;

import java.lang.reflect.Type;
import java.util.*;

/**
 * implements deserialization of church json
 * object in our couchdb
 * thx to http://bit.ly/1iZWjoo
 * @author Jakub Zitny <t102012001@ntut.edu.tw>
 * @since Fri May 16 11:00:11 CST 2014
 */
public class ChurchDeserializer implements JsonDeserializer<Church> {

    /**
     * implements deserialization of couchdb church types
     * TODO: exceptions in param handling
     * @param json
     * @param typeOfT
     * @param context
     * @return deserialized church object
     * @throws JsonParseException
     */
    @Override
    public Church deserialize(final JsonElement json, final Type typeOfT,
            final JsonDeserializationContext context) throws JsonParseException {

        final JsonObject jsonObject = json.getAsJsonObject();

        // fetch string params
        List<String> mandParams = new ArrayList<String>();
        Map<String, String> params = new HashMap<String, String>();
        mandParams.add("_id");
        mandParams.add("_rev");
        mandParams.add("name");
        mandParams.add("shortname");
        mandParams.add("street");
        mandParams.add("city");
        mandParams.add("web");
        mandParams.add("email");

        for (int i = 0; i < mandParams.size(); i++) {
            String paramName = mandParams.get(i);
            String paramValue = null;
            if (jsonObject.has(paramName)) {
                JsonElement paramJson = jsonObject.get(paramName);
                paramValue = (paramJson.isJsonNull()) ? null : paramJson.getAsString();
            }
            params.put(paramName, paramValue);
        }
        // fetch phones
        JsonElement phonesJson = jsonObject.get("phones");
        final List<String> phones = (!phonesJson.isJsonNull()) ? null : jsonArrayToList(phonesJson.getAsJsonArray());
        // fetch schedule
        Schedule schedule = context.deserialize(jsonObject.get("schedule"), Schedule.class);
        // fetch geo
        JsonElement geoJson = jsonObject.get("geo");
        JsonElement latJson = geoJson.getAsJsonObject().get("lat");
        JsonElement lngJson = geoJson.getAsJsonObject().get("long");
        LatLng latLng = new LatLng(latJson.getAsDouble(), lngJson.getAsDouble());
        return Church.create(params.get("_id"), params.get("_rev"), params.get("name"),
                params.get("shortname"), params.get("street"), params.get("city"), phones,
                params.get("web"), params.get("email"), schedule, latLng);
    }

    /**
     * converts jsonArray to list of strings
     * @param jsonArray array to convert
     * @return converted list
     */
    private List<String> jsonArrayToList(JsonArray jsonArray) {
        List<String> list = new ArrayList<String>();
        for (int i = 0 ; i < jsonArray.size(); i++) {
            list.add(jsonArray.get(i).getAsString());
        }
        return list;
    }

}
