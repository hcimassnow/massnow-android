package com.massnow.app.util;

import java.io.InputStream;

/**
 * TODO
 */
public class StringUtil {


    /**
     * stream to string converter
     * thx to http://stackoverflow.com/a/5445161/1893452
     * @param is input stream
     * @return string
     */
    public static String convertStreamToString(InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

}
