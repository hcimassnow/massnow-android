package com.massnow.app;

import android.app.Application;
import com.massnow.app.logic.ChurchListControl;

/**
 * MassNowApplication application child
 * for dispatching the mvc
 * context into controllers
 * @author Jakub Zitny <t102012001@ntut.edu.tw>
 * @since May 19 5:33 AM 2014
 */
public class MassNowApplication extends Application {

    private static MassNowApplication INSTANCE;

    private ChurchListControl mChurchListControl;

    /**
     * singleton for (possible) global objects
     * @return single instance of this class
     */
    public static MassNowApplication getInstance () {
        return INSTANCE;
    }

    /**
     * onCreate callback
     * for application initial setup
     */
    @Override
    public void onCreate () {
        super.onCreate();
        INSTANCE = this;
        mChurchListControl = ChurchListControl.create(this);
    }

    /**
     * used only for testing (not production!)
     * called before killing the app
     * ! not called in production !
     */
    @Override
    public void onTerminate () {
        cleanUp();
        super.onTerminate();
    }

    /**
     * cleans up the stuff that needs to be cleaned up
     * shouldstop is a bit ugly because of http://goo.gl/Jg5dkN
     * TODO: need this? refer http://goo.gl/4ZzJqz
     */
    public void cleanUp () {
        mChurchListControl.clean();
    }


//    //
//    // UNUSED DAGGER STUFF
//    //
//    private ObjectGraph objectGraph;
//
//    /**
//     * initial request to build the graph
//     */
//    @Override
//    public void onCreate() {
//        super.onCreate();
//        objectGraph = ObjectGraph.create(new Object[] { new DaggerModule() });
//        //DaggerModule.getObjectGraph();
//    }
}
