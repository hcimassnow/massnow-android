# MassNow Android app

Our app will provide up-to-date information about churches and mass services they provide. App will be able to show the closest churches, navigate user to their location and provide a smart search where user will be able to input queries like “masses today in prague” or “catholic churches in da-an district”. The goal is to enable anyone to visit any nearby church when they have free time or just search for churches in any city they are in. Primary source of information will be the web service API we will create too. The Android application will be fetching the data for offline use and will regularly check for updates from the API. So as a result users will be able to use the app also without internet connection.

More info in [this presentation](https://docs.google.com/presentation/d/191n5CEAjY1rgdWF-C7Cux_5qRdIBmvWz1quCwF0qe1Q/edit?usp=sharing).

## Development
Project was migrated to gradle and [Android Studio IDE](http://developer.android.com/sdk/installing/studio.html). Everything should work faster and more intuitive.

Just checkout the development branch, build using gradle and you're good to code and test on device.

1. Download Android Studio [here](http://developer.android.com/sdk/installing/studio.html).
2. On welcome screen select `Checkout from Version Control -> Git` (or inside app from menu VCS -> Checkout from Version Control -> Git).
3. Input repository URL `git@bitbucket.org:hcimassnow/massnow-android.git`.
4. Setup directory name "MassNowApp". 
5. Clone.
6. Open the project.
7. Select "Use default Gradle wrapper" and wait for opening and gradle build.
8. Try menu Run -> Run 'app' to test on a device (or AVD or GenyMotion).
9. Switch to `devel` branch
	1. menu VCS -> Git -> Branches
	2. Remote Branches -> origin/devel -> Checkout as new local branch
	3. name it "devel"
10. Do stuff.

### Git setup under Windows

1. Download git for Windows [here](http://msysgit.github.io)
2. Install it with all options selected (be sure to check Git Shell and also Git GUI)
3. Open Git GUI from strt menu and generate ssh keypair
	1. select menu Help -> Show SSH key
	2. click Generate and it will generate the keypair (leave the passphrase empty (two times))
	3. copy the generated public key into your bitbucket account in top right menu Manage account -> SSH keys -> Add key
	4. the public key should look like this - `ssh-dss <longbase64string>== user@host`
4. Open Android Studio and point the git executable to the correct path
	1. From welcome screen go to Configure -> Preferences and there Version Control -> Git
	2. Set path to git executable where you installed it (probably C:\Program files\Git\bin\git.exe)
	3. click test and it should pass for correct path
5. You can happily clone, commit, push and pull everything.

### Android devtools required

In the Android SDK Manager (in Android Studio top bar 3rd icon from right) be sure to install the newest tools, newest SDK API, which is 19, install all needed updates and also additional extras Google Repository, Google Play services, Android Support Repository, Android Support Library.

### Git rules
- work on devel branch
- keep conventions!
- commit only working code
- write comments to each method (with @author directive)

## Useful links

- [Google Drive Shared Folder](http://goo.gl/Vyiweh)
- [Asana Tasklist](http://goo.gl/J3iCdn)
- [App ui mockups](http://bit.ly/1j7gEnQ)

## Features

- Support down to API 10 (Gingerbread)
    - v4, v7
    - fragment, actionbar, searchview support libraries
- OO MVC decoupled design
    - eventbus, butterknife
    - ...